<?php

session_status();

header("Content-Type: image/png");

$image = imagecreate(260 , 100) ;

$background_color = imagecolorallocate($image, 250, 200, 10);

$text_color = imagecolorallocate($image, 100, 100, 250);

$code = rand(1000 , 100000);
$_SESSION ['captcha'] = $code;

//imagestring($image, 40, 100, 50,  "Magenta", $text_color);
imagettftext($image, 40, 0, 26, 70, $text_color, "fonts/font.TTF", $code);

// add some pixels
$pixel_color = imagecolorallocate($image, 5,10,155);
for($i=0;$i<1000;$i++) {
    imagesetpixel($image,rand(0,300),rand(0,100),$pixel_color);
}

// add line color
$line_color = imagecolorallocate($image, 64,64,64);
    for($i=0;$i<10;$i++) {
    imageline($image,40,rand(120,10),rand(120,300),rand(0,50),$line_color);
}

imagepng($image);

imagedestroy($image);



