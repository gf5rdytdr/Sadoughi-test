<?php
if (!isset($_COOKIE["username"]))
    header("location:login.php");
?>

<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="fa">
<![endif]-->
<!--[if (gte IE 9)|!(IE)]>
<html lang="en" class="no-js">
<![endif]-->
<html lang="fa-ir" dir="rtl">
<head>
    <?php
    include "publics.php";
    $name = "Sadoughi";
    ?>
    <title>Sadoughi - Home</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <!-- Page Description and Author -->
    <meta content="Intimate - Bootstrap HTML5 Blog Template" name=
    "description">
    <meta content="GrayGrids" name="author"><!-- Bootstrap Css -->
    <link href="css/bootstrap.min.css" madia="screen" rel="stylesheet" type=
    "text/css"><!-- Font Icon Css -->
    <link href="fonts/font-awesome.min.css" madia="screen" rel="stylesheet"
          type="text/css">
    <link href="fonts/intimate-fonts.css" madia="screen" rel="stylesheet" type=
    "text/css"><!-- Main Css Styles -->
    <link href="css/main.css" madia="screen" rel="stylesheet" type="text/css">
    <!-- Owl Carousel -->
    <link href="extras/owl/owl.carousel.css" media="screen" rel="stylesheet"
          type="text/css">
    <link href="extras/owl/owl.theme.css" media="screen" rel="stylesheet" type=
    "text/css">
    <link href="extras/animate.css" media="screen" rel="stylesheet" type=
    "text/css">
    <link href="extras/lightbox.css" media="screen" rel="stylesheet" type=
    "text/css">
    <link href="extras/slicknav.css" media="screen" rel="stylesheet" type=
    "text/css"><!-- Responsive Css Styles -->
    <link href="css/responsive.css" madia="screen" rel="stylesheet" type=
    "text/css">
</head>
<body>
<!-- Header Section Start -->
<header class="site-header">
    <nav class="navbar navbar-default navbar-intimate role="
         data-offset-top="50" data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <!-- Start Toggle Nav For Mobile -->
                <button class="navbar-toggle" data-target="#navigation"
                        data-toggle="collapse" type="button"><span class=
                                                                   "sr-only">Toggle navigation</span> <span class=
                                                                                                            "icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span></button>
                <div class="logo">
                    <a class="navbar-brand" href="index.php"><i class=
                                                                "fa fa-home"></i></a>
                </div>
            </div><!-- Stat Search -->
            <div class="side">
                <a class="show-search"><i class="ico-search"></i></a>
            </div><!-- Form for navbar search area -->
            <form class="full-search">
                <div class="container">
                    <div class="row">
                        <input class="form-control" placeholder="Search"
                               type="text"> <a class="close-search"><span class=
                                                                          "ico-times"></span></a>
                    </div>
                </div>
            </form><!-- Search form ends -->

            <!-- Navigation Start -->
            <div class="navbar-collapse collapse" id="navigation">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a href="index.php">Home</a>
                    </li>
                    <li class="dropdown dropdown-toggle">
                        <a data-toggle="dropdown" href=
                        "portfolio.html">pages</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">page 01</a>
                            </li>
                            <li>
                                <a href="#">page 02</a>
                            </li>
                            <li>
                                <a href="#">page 03</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-toggle">
                        <a data-toggle="dropdown" href="#">Blog</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">Blog View</a>
                            </li>
                            <li>
                                <a href="#">Single Post</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li>
                        <a href="#">download</a>
                    </li>

                </ul>
                <div id="slog">
                        <span>
                            <?php
                            $title = $_GET['title'];
                            $subtitle = $_GET['subtitle'];
                            echo "<b><i> wellcome </i></b>" . $_COOKIE['username'];

                            ?>
                        </span>
                </div>
            </div><!-- Navigation End -->
        </div>
    </nav><!-- Mobile Menu Start -->
    <ul class="wpb-mobile-menu">
        <li class="active">
            <a href="index.php">Home</a>

        </li>
        <li>
            <a href="#">pages</a>
            <ul>
                <li>
                    <a href="#">page 01</a>
                </li>
                <li>
                    <a href="#">page 02</a>
                </li>
                <li>
                    <a href="#">page 03</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">Blog</a>
            <ul>
                <li>
                    <a href="#">Blog View</a>
                </li>
                <li>
                    <a href="#">Single Post</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">Contact</a>
        </li>
        <li>
            <a href="#">download</a>
        </li>
    </ul><!-- Mobile Menu End -->
</header><!-- Header Section End -->
<!-- Hero Area Start -->
<section class="text-center" id="hero-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="intro-area">
                    <h3>Welcome To</h3>
                    <h2 class="page-title">PHP Learning</h2>
                </div>
            </div>
        </div>
    </div>
</section><!-- Hero Area End -->
<!-- Content Start -->
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="sidebar">
                    <div class="entry-widget">

                        <?php
                        include "menu/login_register";
                        ?>

                    </div>
                    <div class="entry-widget">
                        <h5 class="widget-title">Links</h5>
                        <ul class="archivee">
                            <?php
                            include "menu/menu_asid_left_link";
                            ?>

                        </ul>
                    </div>
                    <div class="entry-widget">
                        <h5 class="widget-title">Categories</h5>
                        <!-- Accordion  -->
                        <div class="accordion">
                            <?php
                            include "menu/menu_asid_left_Categories";
                            ?>

                        </div>
                    </div>

                    <div class="entry-widget">
                        <h5 class="widget-title">Meta</h5>
                        <ul class="meta-list">
                            <li>
                                <a href="#"><i class=
                                               "ico-keyboard_arrow_left"></i> Log In</a>
                            </li>
                            <li>
                                <a href="#"><i class=
                                               "ico-keyboard_arrow_left"></i> Entries
                                    RSS</a>
                            </li>
                            <li>
                                <a href="#"><i class=
                                               "ico-keyboard_arrow_left"></i> Comments
                                    RSS</a>
                            </li>
                            <li>
                                <a href="#"><i class=
                                               "ico-keyboard_arrow_left"></i>
                                    WordPress.org</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-8">


                    <?php
                    $conn = getConnection();
                    $conn->set_charset(utf8);
                    $selectQuery = "SELECT * FROM article";
                    $conn->query($selectQuery);
                    $result = $conn->query($selectQuery);

                    while ($row = $result->fetch_assoc()) {

                        $title = $row['title'];
                        $detail = $row['detail'];
                        $datetime = $row['datetime'];


                        echo <<<EOF
                                
                        <!-- Blog Article Start-->
                        <article>
                        <!-- Blog item Start -->
                        <div class="blog-item-wrap">
                            <!-- Post Format icon Start -->
                            <div class="post-format">
                                <span><i class="fa fa-camera"></i></span>
                            </div><!-- Post Format icon End -->
                            <h2 class="blog-title"><a href="#"> $title </a>
                            </h2><!-- Entry Meta Start-->
                            <div class="entry-meta">
                                <span class="meta-part"><i class="ico-user"></i> <a href="#">کاربر
                                </a></span> <span class="meta-part"><i class="ico-calendar-alt-fill"></i> <a href=
                                "#">$datetime</a></span> <span class="meta-part"><i class="ico-comments"></i>
                                <a href="#">20</a></span> <span class="meta-part"><i class="ico-tag"></i> <a href="#">Tech</a></span>
                                 <span class="meta-part"><i class="ico-star"></i> <a href="#">7.5</a></span>
                            </div><!-- Entry Meta End-->
                            <!-- Feature inner Start -->
                            
                            <!-- Post Content Start -->
                            <div class="post-content">
                                $detail
                            </div><!-- Post Content End -->
                            <div class="entry-more">
                                <div class="pull-left">
                                    <a class="btn btn-common" href=
                                    "single.html">جزئیات <i class=
                                    "ico-arrow-right"></i></a>
                                </div>
                                <div class="share-icon pull-right">
                                    <span class="socialShare"></span>
                                </div>
                            </div>
                        </div><!-- Blog item End -->
                        </article><!-- Blog Article End-->





EOF;


                    }
                    ?>





            </div>
        </div>
    </div>
</div><!-- Content End -->
<!-- Footer Start -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-inner text-center">
                    <div class="social-links">
                        <a class="twitter social-link" data-placement="top"
                           data-toggle="tooltip" href="#" title=
                           "Twitter"><i class="fa fa-twitter"></i></a>
                        <a class="facebook social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "Facebook"><i class="fa fa-facebook"></i></a>
                        <a class="google-plus social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "Google+"><i class="fa fa-google-plus"></i></a>
                        <a class="linkedin social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "LinkedIn"><i class="fa fa-linkedin"></i></a>
                        <a class="dribbble social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "Dribbble"><i class="fa fa-dribbble"></i></a>
                        <a class="pinterest social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "Pinterest"><i class="fa fa-pinterest"></i></a>
                    </div>
                    <ul class="footer-menu">
                        <li class="active">
                            <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="#">Portfolio</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                        <li>
                            <a href="#">download</a>
                        </li>
                    </ul>
                    <div class="copyright">
                        <p>Copyright © 2016 IntimateBlog. Designed and Developed by
                            <a href="?">Dj-SH</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- Footer End -->
<!-- js  -->
<script src="js/jquery-min.js" type="text/javascript">
</script>
<script src="js/bootstrap.min.js" type="text/javascript">
</script>
<script src="js/owl.carousel.js" type="text/javascript">
</script>
<script src="js/jquery.mixitup.min.js" type="text/javascript">
</script>
<script src="js/lightbox.js" type="text/javascript">
</script>
<script src="js/plugin.js" type="text/javascript">
</script>
<script src="js/jquery.slicknav.js" type="text/javascript">
</script>
<script src="js/count-to.js" type="text/javascript">
</script>
<script src="js/jquery.appear.js" type="text/javascript">
</script>
<script src="js//main.js" type="text/javascript">
</script>
</body>
</html>