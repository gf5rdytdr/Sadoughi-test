<?php
if (!isset($_COOKIE["username"]))
    header("location:login.php");
?>
<!DOCTYPE html>
<html lang="fa-ir" dir="rtl" charset="utf-8">
<head>
    <?php
    include "publics.php";
    $name = "Sadoughi";
    ?>
    <title>Sadoughi - users</title>
    <meta http-equiv="Content-Type" content="text/html; charset=" utf-8
    ">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <!-- Page Description and Author -->
    <meta content="Intimate - Bootstrap HTML5 Blog Template" name=
    "description">
    <meta content="GrayGrids" name="author"><!-- Bootstrap Css -->
    <link href="css/bootstrap.min.css" madia="screen" rel="stylesheet" type=
    "text/css"><!-- Font Icon Css -->
    <link href="fonts/font-awesome.min.css" madia="screen" rel="stylesheet"
          type="text/css">
    <link href="fonts/intimate-fonts.css" madia="screen" rel="stylesheet" type=
    "text/css"><!-- Main Css Styles -->
    <link href="css/main.css" madia="screen" rel="stylesheet" type="text/css">
    <!-- Owl Carousel -->
    <link href="extras/owl/owl.carousel.css" media="screen" rel="stylesheet"
          type="text/css">
    <link href="extras/owl/owl.theme.css" media="screen" rel="stylesheet" type=
    "text/css">
    <link href="extras/animate.css" media="screen" rel="stylesheet" type=
    "text/css">
    <link href="extras/lightbox.css" media="screen" rel="stylesheet" type=
    "text/css">
    <link href="extras/slicknav.css" media="screen" rel="stylesheet" type=
    "text/css"><!-- Responsive Css Styles -->
    <link href="css/responsive.css" madia="screen" rel="stylesheet" type=
    "text/css">
</head>
<body>
<!-- Header Section Start -->
<header class="site-header">
    <nav class="navbar navbar-default navbar-intimate role="
         data-offset-top="50" data-spy="affix">
        <div class="container">
            <div class="navbar-header">
                <!-- Start Toggle Nav For Mobile -->
                <button class="navbar-toggle" data-target="#navigation"
                        data-toggle="collapse" type="button"><span class=
                                                                   "sr-only">Toggle navigation</span> <span class=
                                                                                                            "icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span></button>
                <div class="logo">
                    <a class="navbar-brand" href="index.php"><i class=
                                                                 "fa fa-list-ul "></i></a>
                </div>
            </div><!-- Stat Search -->
            <div class="side">
                <a class="show-search"><i class="ico-search"></i></a>
            </div><!-- Form for navbar search area -->
            <form class="full-search">
                <div class="container">
                    <div class="row">
                        <input class="form-control" placeholder="Search"
                               type="text"> <a class="close-search"><span class=
                                                                          "ico-times"></span></a>
                    </div>
                </div>
            </form><!-- Search form ends -->
            <!-- Navigation Start -->
            <div class="navbar-collapse collapse" id="navigation">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="dropdown dropdown-toggle">
                        <a data-toggle="dropdown" href=
                        "portfolio.html">pages</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="portfolio-col-3.html">page 01</a>
                            </li>
                            <li>
                                <a href="portfolio-col-4.html">page 02</a>
                            </li>
                            <li>
                                <a href="portfolio-item.html">page 03</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-toggle">
                        <a data-toggle="dropdown" href="#">Blog</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="blog.html">Blog View</a>
                            </li>
                            <li>
                                <a href="single.html">Single Post</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                    <li>
                        <a href="#">download</a>
                    </li>
                </ul>
            </div><!-- Navigation End -->
        </div>
    </nav><!-- Mobile Menu Start -->
    <ul class="wpb-mobile-menu">
        <li>
            <a href="index.php">Home</a>

        </li>
        <li>
            <a href="#">pages</a>
            <ul>
                <li>
                    <a href="portfolio-col-3.html">page 01</a>
                </li>
                <li>
                    <a href="portfolio-col-4.html">page 02</a>
                </li>
                <li>
                    <a href="portfolio-item.html">page 03</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">Blog</a>
            <ul>
                <li>
                    <a href="blog.html">Blog View</a>
                </li>
                <li>
                    <a href="single.html">Single Post</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="contact.html">Contact</a>
        </li>
        <li>
            <a href="#">download</a>
        </li>
    </ul><!-- Mobile Menu End -->
</header><!-- Header Section End -->
<section id="hero-area">
    <container>
        <row>
            <div class="col-md-12">
                <div class="intro-area">
                    <h3 class="login">Users List</h3>
                </div>

            </div>
        </row>
    </container>
</section>
<!-- Content Start -->
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="sidebar">
                    <div class="entry-widget">

                        <?php
                        include "menu/login_register";
                        ?>

                    </div>
                    <div class="entry-widget">
                        <h5 class="widget-title">Links</h5>
                        <ul class="archivee">
                            <?php
                            include "menu/menu_asid_left_link";
                            ?>

                        </ul>
                    </div>


                </div>
            </div>
            <div class="col-md-8">
                <!-- Blog Article Start-->
                <article>
                    <!-- Blog item Start -->
                    <div class="blog-item-wrap">
                        <!-- Post Format icon Start -->
                        <div class="post-format">
                            <span><i class="fa fa-list-ul"></i></span>
                        </div><!-- Post Format icon End -->
                        <h2 class="blog-title"><a href="#">Users List</a></h2>
                        <hr id="bold">



                        <li style="    list-style: none;
                        background-color: #fdfdfd;
                        padding: 5px;
                        width: 55px;
                        border: 1px solid #ddd;
                        box-shadow: 0 0 1px #7d7d7d;">
                        <a href="?order=desc" ><img style="width: 15px; margin-right: 3px;" src="img/dsen.png" alt=""></a>
                                            
                        <a href="?order=asc" ><img style="width: 15px;" src="img/ase.png" alt=""></a>
                        </li>    
                        <!-- Post Content Start -->
                        <div class="post-content">

                            <table class="users">
                                <?php

                                $conn = getConnection();
                                $orderby = " order by id asc " ;
                                    if( isset($_REQUEST['order'])){
                                    $orderby = " order by id " . $_REQUEST['order'] ;
                                    }
                                $conn->set_charset("utf8");
                                $query = "SELECT * FROM dt1 " . $orderby;
                                $result = $conn->query($query);
                                if ($result->num_rows > 0) {
                                    while ($row = $result->fetch_assoc()) {
                                        $id       = $row ['id'];
                                        $username = $row ['username'];
                                        $password = $row ['password'];
                                        $name     = $row ['name'];

                                        $file_name = "avatars/" .$id. ".jpg";
                                        if (file_exists("$file_name")){

                                            $img = "<img src='$file_name' style='width: 50px; height:50px' />" ;
                                        }else{
                                            $img = '';
                                        }

                                        echo <<<EOF
                        

                        
                        <tr>

                            <td>
                                  $id  
                            </td>
                            <td>
                                  $img 
                            </td>
                            <td>
                                  $name  
                            </td>
                            
                            <td>
                                  $password  
                            </td>
                            <td>
                                  $username  
                            </td>
                            <td>
                                  <a href="destination.php?job=remove_user&id=$id">
                                  <i class="fa fa-trash" aria-hidden="true"></i>
                                  </a>  
                            </td>
                            <td>
                                  <a class="upd" href="update_user.php?id=$id">
                                  <i class="fa fa-pencil" aria-hidden="true"></i>
                                  </a>  
                            </td>
                        
                        </tr>





EOF;


                                    }


                                }

                                ?>

                            </table>

                            <br><br>

                            <form action="destination.php" method="get">

                                <select name="user">

                                    <option value="1">admin</option>
                                    <option value="2">seeid</option>
                                    <option value="3">شمشاد</option>

                                </select>

                            </form>

                        </div><!-- Post Content End -->


                        <div class="entry-more">
                            <div class="pull-left">
                                <a class="btn btn-common" href=
                                "index.php"><i class="fa fa-home"></i></a>
                            </div>

                        </div>
                    </div><!-- Blog item End -->
                </article><!-- Blog Article End-->
                <!-- Post Quote Start -->


            </div>
        </div>
    </div>
</div><!-- Content End -->
<!-- Footer Start -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-inner text-center">
                    <div class="social-links">
                        <a class="twitter social-link" data-placement="top"
                           data-toggle="tooltip" href="#" title=
                           "Twitter"><i class="fa fa-twitter"></i></a>
                        <a class="facebook social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "Facebook"><i class="fa fa-facebook"></i></a>
                        <a class="google-plus social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "Google+"><i class="fa fa-google-plus"></i></a>
                        <a class="linkedin social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "LinkedIn"><i class="fa fa-linkedin"></i></a>
                        <a class="dribbble social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "Dribbble"><i class="fa fa-dribbble"></i></a>
                        <a class="pinterest social-link" data-placement=
                        "top" data-toggle="tooltip" href="#" title=
                           "Pinterest"><i class="fa fa-pinterest"></i></a>
                    </div>
                    <ul class="footer-menu">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="#">Portfolio</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                        <li>
                            <a href="#">download</a>
                        </li>
                    </ul>
                    <div class="copyright">
                        <p>Copyright © 2016 IntimateBlog. Designed and Developed by
                            <a href="?">Dj-SH</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- Footer End -->
<!-- js  -->
<script src="js/jquery-min.js" type="text/javascript">
</script>
<script src="js/bootstrap.min.js" type="text/javascript">
</script>
<script src="js/owl.carousel.js" type="text/javascript">
</script>
<script src="js/jquery.mixitup.min.js" type="text/javascript">
</script>
<script src="js/lightbox.js" type="text/javascript">
</script>
<script src="js/plugin.js" type="text/javascript">
</script>
<script src="js/jquery.slicknav.js" type="text/javascript">
</script>
<script src="js/count-to.js" type="text/javascript">
</script>
<script src="js/jquery.appear.js" type="text/javascript">
</script>
<script src="js//main.js" type="text/javascript">
</script>
</body>
</html>